package edu.neumont.mat305.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Employee {

	int employeeNumber = -1;
	int jobCode = -1;
	List<Sample> samples;
	public Employee(int empId)
	{
		samples = new ArrayList<Sample>();
		this.employeeNumber = empId;
	}
	
	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}


	public int getJobCode() {
		return jobCode;
	}

	public void setJobCode(int jobCode) {
		this.jobCode = jobCode;
	}
	public void addSample(Sample s)
	{
		samples.add(s);
	}
	public List<Sample> getSamples()
	{
		return samples;
	}
}