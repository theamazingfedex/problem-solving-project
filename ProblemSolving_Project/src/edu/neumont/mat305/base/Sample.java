package edu.neumont.mat305.base;

import java.util.Date;

public class Sample {
	int employeeID;
	Date sampleDate;
	SampleType type;
	int jobCode;
	Element e1,e2,e3;
	double e1val,e2val,e3val;
	public int getEmployeeID() {
		return employeeID;
	}
	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}
	public Date getSampleDate() {
		return sampleDate;
	}
	public void setSampleDate(Date sampleDate) {
		this.sampleDate = sampleDate;
	}
	public SampleType getType() {
		return type;
	}
	public void setType(SampleType type) {
		this.type = type;
	}
	public int getJobCode() {
		return jobCode;
	}
	public void setJobCode(int jobCode) {
		this.jobCode = jobCode;
	}
	public Element getE1() {
		return e1;
	}
	public void setE1(Element e1) {
		this.e1 = e1;
	}
	public Element getE2() {
		return e2;
	}
	public void setE2(Element e2) {
		this.e2 = e2;
	}
	public Element getE3() {
		return e3;
	}
	public void setE3(Element e3) {
		this.e3 = e3;
	}
	public double getE1Val() {
		return e1val;
	}
	public void setE1Val(double d) {
		this.e1val = d;
	}
	public double getE2val() {
		return e2val;
	}
	public void setE2val(double e2val) {
		this.e2val = e2val;
	}
	public double getE3val() {
		return e3val;
	}
	public void setE3val(double e3val) {
		this.e3val = e3val;
	}
	public double getLeadValue()
	{
		double value = -1;
		if(e1==Element.Pb)
			value = e1val;
		else if(e2==Element.Pb)
			value = e2val;
		else if(e3==Element.Pb)
			value = e3val;
		return value;
	}
}
