package edu.neumont.mat305.base;

public enum Element {
	Pb("Lead"), As("Arsenic"), Cd("Cadmium"), hB("Hemoglobin"), ZPP("Zinc Protoporphyrin");
	public String fullName;
	Element(String fullName)
	{
		this.fullName = fullName;
	}
	public static Element getElement(int e)
	{
		switch(e)
		{
		case 1:
			return Pb;
		case 2:
			return Cd;
		case 4:
			return As;
		case 6:
			return hB;
		case 30:
			return ZPP;
		default:
			return null;
		}
	}
}
