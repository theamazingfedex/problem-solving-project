package edu.neumont.mat305.base;

public enum SampleType
{
	Blood, Urine;
	public static SampleType getSampleType(String s)
	{
		if(s.equals("B"))
			return Blood;
		else if(s.equals("U"))
			return Urine;
		else
			return null;
	}
}