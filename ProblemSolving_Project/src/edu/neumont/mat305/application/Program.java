package edu.neumont.mat305.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.swing.text.DateFormatter;

import edu.neumont.mat305.base.Element;
import edu.neumont.mat305.base.Employee;
import edu.neumont.mat305.base.Sample;
import edu.neumont.mat305.base.SampleType;


public class Program 
{
	InputStream fileStream; 
	File inputFile = new File("src/data.txt");
	Scanner inputScanner = new Scanner(System.in);
	List<Sample> samples;
	List<Employee> employees;
	private static final int JOBCODE_ID = 3;
	private static final int EMPLOYEEID_ID = 0;
	private static final int DATE_ID = 1;
	private static final int NUM_DATE_CHARS = 8;
	private static final String LEAD_CODE = "1";
	private static final int ELEMENT_2_ID = 6;
	private static final int ELEMENT_3_ID = 8;
	private static final int ELEMENT_1_ID = 4;
	private static final int ELEMENT_1_VALUE = 5;
	private static final int ELEMENT_2_VALUE = 7;
	private static final int ELEMENT_3_VALUE = 9;
	private static final int SAMPLETYPE_ID = 2;
	Dictionary<Employee,Integer> employeeDaysOnLeave;
	public static void main(String[] args) 
	{
		new Program();
		
	}
	public Program()
	{
		samples = new ArrayList<Sample>();
		employees = new ArrayList<Employee>();
		employeeDaysOnLeave = new Hashtable<Employee,Integer>();
		parseFile();
		List<Sample> impSamples = getImportantSamples();
		for(Sample s: impSamples)
		{
			setEmployees(s);
		}
		for(Employee emp : employees)
		{
			int daysOnLeave = getDaysOnLeave(emp);
			employeeDaysOnLeave.put(emp, daysOnLeave);
		}
		printResults();
	}
	private void printResults() {
		for(Employee e :employees)
		{
			int daysOff = employeeDaysOnLeave.get(e);
			System.out.println("Employee "+e.getEmployeeNumber()+" will be on leave for "+daysOff+" days.");
		}
	}
	private int getDaysOnLeave(Employee emp) {
		int daysOff = 0;
		List<Date> datesOnLeave = getDatesOnLeave(emp);
		List<Date> datesBack = getDatesBack(emp);
//		List<Date> allDates = new ArrayList<Date>();
//		allDates.addAll(datesBack);
//		allDates.addAll(datesOnLeave);
//		Collections.sort(allDates);
		for(int x = 0; x<emp.getSamples().size();x++)
		{
			Sample first = emp.getSamples().get(x);
			if(datesOnLeave.contains(first.getSampleDate()))
			{
				for(int y = x; y<emp.getSamples().size();y++)
				{
					Sample second = emp.getSamples().get(y);
					if(datesBack.contains(second.getSampleDate()))
					{
						daysOff += (int)((second.getSampleDate().getTime() - first.getSampleDate().getTime()) / (1000 * 60 * 60 * 24));
						x=y;
						break;
					}
				}
			}
		}
		return daysOff;
	}
	public List<Sample> getImportantSamples()
	{
		List<Sample> impSamples = new ArrayList<Sample>();
		for(Sample samp : samples)
		{
			if(samp.getLeadValue()>=0&&samp.getType()==SampleType.Blood)
			{
				impSamples.add(samp);
			}
		}
		return impSamples;
	}
	private List<Date> getDatesOnLeave(Employee emp) {
		List<Date> dates = new ArrayList<Date>();
		for(int x = 0; x<emp.getSamples().size()-1; x++)
		{
			if(emp.getSamples().get(x).getLeadValue()>=70 &&
					emp.getSamples().get(x+1).getLeadValue()>=70)
			{
				dates.add(emp.getSamples().get(x+1).getSampleDate());
			}
		}
		return dates;
	}
	private List<Date> getDatesBack(Employee emp) {
		List<Date> dates = new ArrayList<Date>();
		for(int x = 0; x<emp.getSamples().size()-1; x++)
		{
			if(emp.getSamples().get(x).getLeadValue()<=50 &&
					emp.getSamples().get(x+1).getLeadValue()<=50)
			{
				dates.add(emp.getSamples().get(x+1).getSampleDate());
			}
		}
		return dates;
	}
	public void parseFile()
	{
		try 
		{
			fileStream = new FileInputStream(inputFile);
			Scanner fileScanner = new Scanner(fileStream);
			String line = "";
			while (fileScanner.hasNextLine())
			{
				line = fileScanner.nextLine();
				System.out.println(line);
				Sample sample = new Sample();
				String[] sampleString = line.split("\t");
				sample.setEmployeeID(Integer.parseInt(sampleString[EMPLOYEEID_ID]));
				String dateString = sampleString[DATE_ID].length()==NUM_DATE_CHARS?sampleString[DATE_ID]:"0"+sampleString[DATE_ID];
				sample.setSampleDate(new SimpleDateFormat("MMddyyyy").parse(dateString));
				sample.setType(SampleType.getSampleType(sampleString[SAMPLETYPE_ID]));
				sample.setJobCode(Integer.parseInt(sampleString[JOBCODE_ID]));
				sample.setE1(Element.getElement(Integer.parseInt(sampleString[ELEMENT_1_ID])));
				sample.setE1Val(Double.parseDouble(sampleString[ELEMENT_1_VALUE]));
				if(sampleString.length>6)
				{
					sample.setE2(Element.getElement(Integer.parseInt(sampleString[ELEMENT_2_ID])));
					sample.setE2val(Double.parseDouble(sampleString[ELEMENT_2_VALUE]));
					if(sampleString.length>8)
					{
					sample.setE3(Element.getElement(Integer.parseInt(sampleString[ELEMENT_3_ID])));
					sample.setE3val(Double.parseDouble(sampleString[ELEMENT_3_VALUE]));
					}
				}
				samples.add(sample);
				
			}
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		
		
	}
	private void setEmployees(Sample sample) {
		
		boolean found = false;
		for(int x = 0; x <employees.size(); x++)
		{
			if(sample.getEmployeeID()==employees.get(x).getEmployeeNumber())
			{
				found = true;
				employees.get(x).addSample(sample);
			}
		}
		if(!found)
		{
			Employee e = new Employee(sample.getEmployeeID());
			e.addSample(sample);
			employees.add(e);	
		}
	}
}
